/*
 * project: CUnitTest
 * created: Thu Jul 26 13:33:25 2018
 * creator: christian
*/

#include "../src/sum.h"
#include <check.h>
#include <stdio.h>
#include <stdlib.h>

START_TEST(test_sum) {

    extern int sum(int, int);
    ck_assert_int_eq(sum(1, 2), 3);

} END_TEST


Suite *sum_suit() {
    Suite *s;
    TCase *tc_core;
    s = suite_create("sum");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_sum);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
  int no_failed = 0;
  Suite *s;
  SRunner *runner;

  s = sum_suit();
  runner = srunner_create(s);

  srunner_run_all(runner, CK_NORMAL);
  no_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
