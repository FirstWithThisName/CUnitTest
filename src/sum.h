#ifndef _SRC
#define _SRC

/*
 * project: CUnitTest
 * created: Thu Jul 26 13:33:06 2018
 * creator: christian
*/


/**************** START PROTOTYPING ****************/

/**
 * @param a
 * @param b
 * @return summ of a and b.
 */
int sum(int a, int b);

/**************** END   PROTOTYPING ****************/

#endif /* src/sum.h */
