/*
 * project: CUnitTest
 * created: Thu Jul 26 13:32:42 2018
 * creator: christian
*/

#include "sum.h"
#include <stdio.h>

int main(int argc, char *argv[]) {

	printf("sum of 6 and 9 is %d\n", sum(6, 9));

	return 0;
}
