# CUnitTest

## This is a example project to create a Unittest for c using [check](https://libcheck.github.io/check/).

#### Required Software
* check
* gcc
* make

## This is also a test for gitlab piplining
[This was my example](http://www.ccs.neu.edu/home/skotthe/classes/cs5600/fall/2015/labs/intro-check.html) for the yml script.
